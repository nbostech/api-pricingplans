package com.wavelabs.pricigplansandpromotions.model.test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.wavelabs.pricingplansandpromotions.model.Plan;
import com.wavelabs.pricingplansandpromotions.model.PromoCode;
import com.wavelabs.pricingplansandpromotions.model.User;

public class UserTest {
	
	private static final double DELTA = 1e-15;

	@Test
	public void testGetId() {
		User user = new User();
		user.setId(12);
		Assert.assertEquals(12, user.getId());
	}

	@Test
	public void testGetPlan() {
		User user = new User();
		Plan plan = new Plan();
		user.setPlan(plan);
		Assert.assertEquals(plan, user.getPlan());
	}

	@Test
	public void testGetNextSubscriptionPlan() {
		User user = new User();
		Plan plan = new Plan();
		user.setNextSubscriptionPlan(plan);
		Assert.assertEquals(plan, user.getNextSubscriptionPlan());
	}

	@Test
	public void testGetPlanRenewalDate() {
		User user = new User();
		user.setPlanRenewalDate(new Date(1403685556000L));
		Assert.assertEquals(new Date(1403685556000L), user.getPlanRenewalDate());
	}

	@Test
	public void testGetFreeTrialExpiryDate() {
		User user = new User();
		user.setFreeTrialExpiryDate(new Date(1403685556000L));
		Assert.assertEquals(new Date(1403685556000L), user.getFreeTrialExpiryDate());
	}

	@Test
	public void testGetFreeTrialPromoCode() {
		User user = new User();
		PromoCode promoCode = new PromoCode();
		user.setFreeTrialPromoCode(promoCode);
		Assert.assertEquals(promoCode, user.getFreeTrialPromoCode());
	}

	@Test
	public void testGetUsedPromoCodes() {
		User user = new User();
		PromoCode promoCode1 = new PromoCode();
		PromoCode promoCode2 = new PromoCode();
		PromoCode promoCode3 = new PromoCode();
		Set<PromoCode> promoCodes = new HashSet<PromoCode>();
		promoCodes.add(promoCode1);
		promoCodes.add(promoCode2);
		promoCodes.add(promoCode3);
		user.setUsedPromoCodes(promoCodes);
		Assert.assertEquals(promoCodes, user.getUsedPromoCodes());

	}
	
	@Test
	public void testGetAmountPaidForPresentPlan(){
		User user = new User();
		user.setAmountPaidForPresentPlan(500.0);
		Assert.assertEquals(500.0, user.getAmountPaidForPresentPlan(), DELTA);
	}

}
