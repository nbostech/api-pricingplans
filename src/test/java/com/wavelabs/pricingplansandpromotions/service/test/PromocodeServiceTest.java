package com.wavelabs.pricingplansandpromotions.service.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.pricingplansandpromotions.model.PromoCode;
import com.wavelabs.pricingplansandpromotions.repository.PromoCodeRepository;
import com.wavelabs.pricingplansandpromotions.service.PromoCodeService;


@RunWith(MockitoJUnitRunner.class)
public class PromocodeServiceTest {
	
	@Mock
	PromoCodeRepository promoCodeRepo;

	@InjectMocks
	PromoCodeService promoCodeService;

	/*@Test
	public void testAddPromoCode() {
		PromoCode promoCode = DataBuilder.getPromoCode();
		when(promoCodeRepo.save(any(PromoCode.class))).thenReturn(promoCode);
		PromoCode promoCode1 = promoCodeService.addPromoCode(promoCode);
		Assert.assertEquals(promoCode, promoCode1);
	}*/
	
	@Test
	public void testGetPromoCode(){
		PromoCode promoCode = DataBuilder.getPromoCode();
		when(promoCodeRepo.findOne(anyInt())).thenReturn(promoCode);
		PromoCode promoCode1 = promoCodeService.getPromoCode(1);
		Assert.assertEquals(promoCode, promoCode1);
	}
}
