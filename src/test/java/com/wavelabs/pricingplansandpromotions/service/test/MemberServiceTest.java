package com.wavelabs.pricingplansandpromotions.service.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.pricingplansandpromotions.model.User;
import com.wavelabs.pricingplansandpromotions.repository.UserRepository;
import com.wavelabs.pricingplansandpromotions.service.UserService;


@RunWith(MockitoJUnitRunner.class)
public class MemberServiceTest {

	@Mock
	UserRepository userRepo;

	@InjectMocks
	UserService userService;
	
	@Mock
	EntityManager entityManager;


	@Test
	public void testGetUser() {
		User user = DataBuilder.getUser();
		when(userRepo.findOne(anyInt())).thenReturn(user);
		User user1 = userService.getUser(1);
		Assert.assertEquals(user.getId(), user1.getId());
	}
	
	@Test
	public void testUpdateUser(){
		User user = DataBuilder.getUser();
		when(entityManager.merge(any(User.class))).thenReturn(user);
		User user1 = userService.updateUser(user);
		Assert.assertEquals(user.getId(), user1.getId());
	}
}
