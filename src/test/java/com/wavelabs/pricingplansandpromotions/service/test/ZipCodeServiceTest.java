package com.wavelabs.pricingplansandpromotions.service.test;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.wavelabs.pricingplansandpromotions.model.ZipCode;
import com.wavelabs.pricingplansandpromotions.repository.ZipCodeRepository;
import com.wavelabs.pricingplansandpromotions.service.SchemeService;
import com.wavelabs.pricingplansandpromotions.service.ZipCodeService;

@RunWith(MockitoJUnitRunner.class)
public class ZipCodeServiceTest {

	@Mock
	ZipCodeRepository zipCodeRepo;
	
	@InjectMocks
	ZipCodeService zipCodeService;
	
	@InjectMocks
	SchemeService schemeService;

	/*@Test
	public void testAddZipCode() {
		ZipCode zipCode = DataBuilder.getZipCode();
		when(zipCodeRepo.save(zipCode)).thenReturn(zipCode);
		ZipCode zipCode1 = zipCodeService.addZipCode(zipCode);
		Assert.assertEquals(zipCode, zipCode1);
	}*/
	
	@Test
	public void testGetZipCode(){
		ZipCode zipCode = DataBuilder.getZipCode();
		when(zipCodeRepo.findOne(anyInt())).thenReturn(zipCode);
		ZipCode zipCode1 = zipCodeService.getZipCode(1);
		Assert.assertEquals(zipCode.getId(), zipCode1.getId());
	}
	
}
