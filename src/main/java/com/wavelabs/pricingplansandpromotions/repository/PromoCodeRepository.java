package com.wavelabs.pricingplansandpromotions.repository;

import org.springframework.data.repository.CrudRepository;

import com.wavelabs.pricingplansandpromotions.model.PromoCode;

public interface PromoCodeRepository extends CrudRepository<PromoCode, Integer>{

	PromoCode findByCode(String promoCode);
}
