package com.wavelabs.pricingplansandpromotions.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "plan")
public class Plan {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "name", unique = true)
	private String name;

	@Column(name = "price")
	private double price;

	@Column(name = "duration_in_months")
	private int durationInMonths;

	@Column(name = "no_of_days_for_free_trial")
	private int noOfDaysForFreeTrial;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "scheme_id", nullable = false)
	private Scheme scheme;

	public Plan() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getDurationInMonths() {
		return durationInMonths;
	}

	public void setDurationInMonths(int durationInMonths) {
		this.durationInMonths = durationInMonths;
	}

	public int getNoOfDaysForFreeTrial() {
		return noOfDaysForFreeTrial;
	}

	public void setNoOfDaysForFreeTrial(int noOfDaysForFreeTrial) {
		this.noOfDaysForFreeTrial = noOfDaysForFreeTrial;
	}

	public Scheme getScheme() {
		return scheme;
	}

	public void setScheme(Scheme scheme) {
		this.scheme = scheme;
	}
}
