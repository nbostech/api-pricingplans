package com.wavelabs.pricingplansandpromotions.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "scheme")

@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class, 
		  property = "id")
public class Scheme {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "name",unique=true)
	private String name;
	
	@Column(name = "is_active", nullable=false)
	private byte isActive;
	
	@Column(name = "free_trial", nullable=false)
	private byte freeTrial;
	
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "scheme",fetch = FetchType.LAZY)
	private Set<Plan> plans;

	public Scheme() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte getIsActive() {
		return isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public byte getFreeTrial() {
		return freeTrial;
	}

	public void setFreeTrial(byte freeTrial) {
		this.freeTrial = freeTrial;
	}

	public Set<Plan> getPlans() {
		return plans;
	}

	public void setPlans(Set<Plan> plans) {
		this.plans = plans;
	}

}
