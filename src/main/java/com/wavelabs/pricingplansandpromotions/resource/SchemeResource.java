package com.wavelabs.pricingplansandpromotions.resource;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.pricingplansandpromotions.model.Scheme;
import com.wavelabs.pricingplansandpromotions.service.SchemeService;

import io.nbos.capi.api.v0.models.NotFoundResponse;

@RestController
@Component
public class SchemeResource {
	@Autowired
	SchemeService schemeService;
	private final static Logger LOGGER = Logger.getLogger(SchemeResource.class.getName());

	@RequestMapping(value = "api/pricingplans/v1/schemes/{id}", produces = MediaType.APPLICATION_JSON_VALUE)

	public ResponseEntity<Object> getScheme(@PathVariable("id") int schemeId) {
		Scheme scheme = schemeService.getScheme(schemeId);
		if (scheme != null) {
			return ResponseEntity.status(200).body(scheme);
		} else {
			NotFoundResponse restMessage = new NotFoundResponse();
			restMessage.message = "Scheme not Found";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		}
	}

	@RequestMapping(value = "api/pricingplans/v1/schemes", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_HTML_VALUE }, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Scheme> addScheme(@RequestBody Scheme scheme) {
		return ResponseEntity.status(200).body(schemeService.addScheme(scheme));

	}

	@RequestMapping(value = "api/pricingplans/v1/schemes/randomscheme", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getRandomScheme(@RequestParam("zipCodeId") int zipCodeId,
			@RequestParam("userId") int userId) {
		try {
			Scheme scheme = schemeService.getRandomScheme(zipCodeId, userId);
			return ResponseEntity.status(200).body(scheme);

		} catch (NullPointerException ne) {
			LOGGER.log(Level.SEVERE, "Exception: ", ne);
			NotFoundResponse restMessage = new NotFoundResponse();
			restMessage.message = "Zip code not Found";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		}
	}

}
