package com.wavelabs.pricingplansandpromotions.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.pricingplansandpromotions.model.TokenInfo;
import com.wavelabs.pricingplansandpromotions.model.ZipCode;
import com.wavelabs.pricingplansandpromotions.service.ZipCodeService;
import com.wavelabs.pricingplansandpromotions.utility.UserTypeChecker;

import io.nbos.capi.api.v0.models.NotFoundResponse;
import io.nbos.capi.api.v0.models.RestMessage;

@RestController
@Component
public class ZipCodeResource {

	@Autowired
	ZipCodeService zip;

	private static String forbiddenMessage = "Please Login";

	@RequestMapping(value = "api/pricingplans/v1/zipcodes/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object> getZipCode(@PathVariable("id") int id) {
		ZipCode zipCode = zip.getZipCode(id);
		if (zipCode != null) {
			return ResponseEntity.status(200).body(zipCode);
		} else {
			NotFoundResponse restMessage = new NotFoundResponse();
			restMessage.message = "Zip code not Found";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "api/pricingplans/v1/zipcodes", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_HTML_VALUE }, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity addZipCode(@RequestBody ZipCode zipCode, @RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean guestUserFlag = UserTypeChecker.isGuestUser(tokenInfo);
		if (!guestUserFlag) {
			boolean flag = zip.addZipCode(zipCode);
			RestMessage restMessage = new RestMessage();
			if (flag) {
				restMessage.message = "Zip code created Successfully!";
				restMessage.messageCode = "200";
				return ResponseEntity.status(200).body(restMessage);
			} else {
				restMessage.messageCode = "500";
				restMessage.message = "Plan creation failed!";
				return ResponseEntity.status(500).body(restMessage);
			}
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}

	@RequestMapping(value = "api/pricingplans/v1/zipcodes/assignscheme", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.TEXT_HTML_VALUE }, method = RequestMethod.PUT)
	public ResponseEntity<ZipCode> addSchemeToZip(@RequestParam("schemeId") int schemeId,
			@RequestParam("zipCodeId") int zipCodeId) {
		return ResponseEntity.status(200).body(zip.addSchemeToZip(schemeId, zipCodeId));
	}
}
