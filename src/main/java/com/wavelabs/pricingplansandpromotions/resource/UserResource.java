package com.wavelabs.pricingplansandpromotions.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.pricingplansandpromotions.model.TokenInfo;
import com.wavelabs.pricingplansandpromotions.model.User;
import com.wavelabs.pricingplansandpromotions.service.UserService;
import com.wavelabs.pricingplansandpromotions.utility.UserTypeChecker;

import io.nbos.capi.api.v0.models.InternalErrorResponse;
import io.nbos.capi.api.v0.models.NotFoundResponse;
import io.nbos.capi.api.v0.models.RestMessage;
import io.nbos.capi.api.v0.models.SuccessResponse;

@RestController
@Component
public class UserResource {

	private static String forbiddenMessage = "Please Login";

	@Autowired
	UserService userService;

	@RequestMapping(value = "api/pricingplans/v1/users/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<Object> getUser(@PathVariable("id") int id,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean guestUserFlag = UserTypeChecker.isGuestUser(tokenInfo);
		if (!guestUserFlag) {
			User user = userService.getUser(id);
			if (user != null) {
				return ResponseEntity.status(200).body(user);

			} else {
				NotFoundResponse restMessage = new NotFoundResponse();
				restMessage.message = "User not Found";
				restMessage.messageCode = "404";
				return ResponseEntity.status(404).body(restMessage);
			}
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "api/pricingplans/v1/users/{id}", method = RequestMethod.DELETE)
	public ResponseEntity deleteUser(@PathVariable("id") int userId,
			@RequestAttribute("tokenInfo") TokenInfo tokenInfo) {
		boolean guestUserFlag = UserTypeChecker.isGuestUser(tokenInfo);
		if (!guestUserFlag) {
			User user = userService.deleteUser(userId);
			if (user != null) {
				SuccessResponse success = new SuccessResponse();
				success.message = "User deleted successfully!";
				success.messageCode = "200";
				return ResponseEntity.status(200).body(success);
			} else {
				InternalErrorResponse errorResponse = new InternalErrorResponse();
				errorResponse.message = "User deletion Failed!";
				errorResponse.messageCode = "500";
				return ResponseEntity.status(500).body(errorResponse);
			}
		} else {
			RestMessage restMessage = new RestMessage();
			restMessage.messageCode = "403";
			restMessage.message = forbiddenMessage;
			return ResponseEntity.status(403).body(restMessage);
		}
	}
}
