Description:
This application provides a platform to create and manage pricing plans, promocodes for a website. User can subscribe plans, free trial and apply promo codes to get discount.

Features:
1.Creating zip codes.
2.Creating a Scheme(Set of plans).
3.Creating a Plan and adding to a scheme.
4.Creating a Free trial and adding to a scheme.
5.Assigning a scheme to zip codes.
6.Creating a Promo code which can be used only once by a user and having expiry date.
7.Randomly picking a scheme from the schemes allotted to zip code.
8.Checking whether the promo code entered is valid, expired, already used by customer and displaying the final price deducting the discount after entering the Promo code.
9.Subscribing for a plan or a free trial.
10.Auto renewal of the plan on reaching the renewal date and Auto Upgrading the free trial to the plan on reaching free trial expiry date.